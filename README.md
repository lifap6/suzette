# Suzette

Un robot pour Rocket.Chat avec un don pour le Scrabble.

## Exercices

Suzette est en mesure de fournir des exercices type aléatoires pour l'UE Lifap6.
Pour lui en demander, utilisez la syntaxe

```
suzette exo <type d'exercice> [ticket optionnel]
```

où type d'exercice peut être (au moment de la rédaction de cette page) :
* avl (pour un exercice d'insertion dans un AVL)
* tas (pour un exercice d'insertion dans un tas)
* boucles (pour un exercice de calcul de complexité de boucles imbriquées)
* mthm (pour un exercice de calcul de complexité diviser pour régner)

Le ticket permet de retrouver des exercices : à un ticket correspond un
exercice. Si le ticket n'est pas fourni, il est généré automatiquement.
N'importe quelle chaîne de carractères convient.

La correction des exercices peut être obtenue via

```
suzette solution <type d'exercice> <ticket>
```

## Sondage

Suzette a une fonctionnalité de sondages basique. Si elle écoute dans un canal
et qu'elle détecte la chaîne `oui ou non` dans un message posté, elle rajoutera
automatiquement des réactions à ce message pour permettre de voter en cliquant
* pouce vers le haut
* pouce vers le bas
* je ne sais pas

Il est également possible de faire des sondages plus complexes en utilisant la
syntaxe :

```
<texte du sondage> au choix : <option 1> ; <option 2> ...
```

Dans ce cas elle forgera un nouveau message où les options seront numérotées de
0 à 9 (10 options maximum donc), et ajoutera à ce message les réactions
numérotées à cliquer.

## Scrabble

Suzette a un don pour le Scrabble. Vous pouvez lui demander le meilleur coup
qu'elle saurait jouer via :

```
suzette scrabble <plateau> <lettres>
```

Le plateau est à fournir au format exporté par la méthode `Board::save` dans
[le code de base du projet
Scrabble](https://forge.univ-lyon1.fr/lifap6/Scrabble-etu).

## Contribuer

Pour contribuer, vous pouvez ajouter de nouvelles interactions en ajoutant des
fichiers dans le dossier scripts. Pour une documentation, Suzette est basée sur
le logiciel [hubot](https://hubot.github.com).

