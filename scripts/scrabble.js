// Description:
//   Solving Scrabble duplicate problems
//
// Dependencies:
//   None
//
// Configuration:
//   None
//
// Commands:
//   hubot scrabble <board> <letters> - < solves a scrabble duplicate problem for you >
//
// Notes:
//   Version 1.0
//
// Author:
//   Vincent Nivoliers

module.exports = (robot) => {

  robot.respond(/scrabble ([\r\n.A-Z]+)\s(\S+)/i, (res) => {
    console.log("handling scrabble query")
    robot.http(`http://localhost:3389/solve?board=${res.match[1]}&rack=${res.match[2]}`).get()( (err, resp, body) => {
      if(resp.statusCode != 200) {
        res.reply(`Nope : ${body}`) ;
      } else {
        let data = JSON.parse(body) ;
        let answer = "" ;
        let init_board = res.match[1].replace(/[^.A-Z]/g,'') ;
        let init_lines = init_board.match(/(.){15}/g) ;
        let result_lines = data.board.match(/(.){15}/g) ;
        answer += "Sur ce plateau :\n\n```\n" ;
        init_lines.forEach( (l) => {
          answer += l ;
          answer += "\n" ;
        }) ;
        answer += `\n\`\`\`\n\n Avec les lettres \`${res.match[2]}\` je marque ${data.score} points :\n\n` ;
        answer += "```\n" ;
        result_lines.forEach( (l) => {
          answer += l ;
          answer += "\n" ;
        }) ;
        answer += "\n```" ;
        res.send(answer) ;
      }
    })
  })
}
