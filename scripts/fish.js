// Description:
//    April fishes.
//
// Dependencies:
//   None
//
// Configuration:
//   None
//
// Commands:
//   None
//
// Notes:
//   Version 1.0
//
// Author:
//   Vincent Nivoliers

module.exports = (robot) => {

  /*
  robot.hear(/^.*$/, (res) => {
    //pickable fishes
    let fishes = [":blowfish:", ":fish:", ":tropical_fish:", ":shark:"] ;
    //probability of getting the fish (sum should stay below 1)
    let probas = [0.05, 0.2, 0.1, 0.05] ;
    //randomly get a number in [0,1]
    let r = Math.random() ;
    //determine whether a fish should be given
    let accu = 0 ;
    for(let i = 0; i < fishes.length; ++i) {
      accu += probas[i] ;
      if(r < accu) {
        //give a fish !
        let id = res.message.id ;
        robot.adapter.driver.setReaction(fishes[i], id) ;
        break ;
      }
    }
  })
  */
}
