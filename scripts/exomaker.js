// Description:
//   Providing algorithmics exercises
//
// Dependencies:
//   None
//
// Configuration:
//   None
//
// Commands:
//   hubot exo boucles ticket - < generate dummy loops exercise for iteration counting >
//   hubot solution boucles ticket - < solve iteration counting for dummy loop exercises >
//   hubot exo mthm ticket - < generate dummy master theorem exercises >
//   hubot solution mthm ticket - < solve dummy master theorem exercises >
//   hubot exo avl ticket - < generate AVL insertion exercises >
//   hubot solution avl ticket - < solve AVL insertion exercises >
//   hubot exo tas ticket - < generate Heap insertion exercises >
//   hubot solution tas ticket - < solve Heap insertion exercises >
//   hubot exo conversion binaire ticket - < generate binary conversion exercises >
//   hubot solution conversion binaire ticket - < solve binary conversion exercises >
//   hubot exam ticket - < generate an exam sheet with mthm, avl and heap >
//
// Notes:
//   Version 1.0
//
// Author:
//   Vincent Nivoliers

const tmp = require('tmp')
const fs = require('fs')

const upload = (robot, room, data) => {
  //data layout
  //{
  //  contents : filedata to upload
  //  format : format of the data, passed to Buffer.from
  //  mime : mime type of the data
  //  filename : name of the destination file on the chat
  //  options : {
  //    key : value
  //  }
  //}
  
  //thanks to https://gist.github.com/tanaikech/40c9284e91d209356395b43022ffc5cc

  //build formdata for the data
  const uid = require('uid-safe') ;
  let boundary = uid.sync(18) ;
  let form = "" ;

  //options
  if(data.options) {
    for(const opt in data.options) {
      if({}.hasOwnProperty.call(data.options, opt)){
        form += `--${boundary}\r\n` ;
        form += `Content-Disposition: form-data; name="${opt}";\r\n\r\n${data.options[opt]}\r\n` ;
      }
    }
  }

  //file upload
  form += `--${boundary}\r\n` ;
  form += `Content-Disposition: form-data; name="file"; filename=${data.filename}\r\n` ;
  form += `Content-Type: ${data.mime}\r\n\r\n` ;

  const payload = Buffer.concat([
    Buffer.from(form, 'utf-8'),
    Buffer.from(data.contents, data.format),
    Buffer.from(`\r\n--${boundary}--\r\n`, 'utf-8')
  ])

  //not able to post fully through robot.adapter.api, the payload is messed up
  //api login
  robot.adapter.api.login().then( (ack) => {
    //api upload
    robot.http(robot.adapter.api.url)
    .header('X-User-Id', ack.data.userId)
    .header('X-Auth-Token', ack.data.authToken)
    .path(`rooms.upload/${room}`)
    .header('Content-Type', `multipart/form-data; boundary=${boundary}`)
    .post(payload)((err, response, body) => {
      if(err) {
        error.log(err) ;
      }
    })
  }) ;
}

const upload_base64_png = (robot, room, data) => {
  data.format = data.format || 'base64' ;
  data.mime = data.mime || 'image/png' ;
  data.filename = data.filename || "image.png" ;
  upload(robot, room, data) ;
}

const exomaker_query_image = (robot, room, hook, ticket) => {
  let query_url = `http://localhost:5000/${hook}`
  if(ticket.length > 0) {
    query_url = `${query_url}?ticket=${ticket}`
  }
  robot.http(query_url).get()( (err, resp, body) => {
    if(err) {
      console.error(err) ;
      return ;
    }
    answer = JSON.parse(body) ;
    ticket = answer.ticket ;
    image = answer.data ;
    upload_base64_png(robot, room, {
      contents : image,
      options : {
        msg : `ticket : \`${ticket}\``
      }
    }) ;
    console.log(`generated ${hook} with ticket ${ticket}`) ;
  })
}

module.exports = (robot) => {

  robot.respond(/exo boucles\s*([^\s]*)/i, (res) => {
    room = res.envelope.user.roomID
    ticket = res.match[1]
    exomaker_query_image(robot, room, "loops.create", ticket) ;
  })

  robot.respond(/solution boucles\s*([^\s]*)/i, (res) => {
    room = res.envelope.user.roomID
    ticket = res.match[1]
    if(ticket.length > 0) {
      exomaker_query_image(robot, room, "loops.solve", ticket) ;
    } else {
      res.send("il faut fournir un ticket pour obtenir une correction") ;
    }
  })

  robot.respond(/exo mthm\s*([^\s]*)/i, (res) => {
    room = res.envelope.user.roomID
    ticket = res.match[1]
    exomaker_query_image(robot, room, "mthm.create", ticket) ;
  })

  robot.respond(/solution mthm\s*([^\s]*)/i, (res) => {
    room = res.envelope.user.roomID
    ticket = res.match[1]
    if(ticket.length > 0) {
      exomaker_query_image(robot, room, "mthm.solve", ticket) ;
    } else {
      res.send("il faut fournir un ticket pour obtenir une correction") ;
    }
  })

  robot.respond(/exo avl\s*([^\s]*)/i, (res) => {
    room = res.envelope.user.roomID
    ticket = res.match[1]
    exomaker_query_image(robot, room, "avl.create", ticket) ;
  })

  robot.respond(/solution avl\s*([^\s]*)/i, (res) => {
    room = res.envelope.user.roomID
    ticket = res.match[1]
    if(ticket.length > 0) {
      exomaker_query_image(robot, room, "avl.solve", ticket) ;
    } else {
      res.send("il faut fournir un ticket pour obtenir une correction") ;
    }
  })

  robot.respond(/exo tas\s*([^\s]*)/i, (res) => {
    room = res.envelope.user.roomID
    ticket = res.match[1]
    exomaker_query_image(robot, room, "heap.create", ticket) ;
  })

  robot.respond(/solution tas\s*([^\s]*)/i, (res) => {
    room = res.envelope.user.roomID
    ticket = res.match[1]
    if(ticket.length > 0) {
      exomaker_query_image(robot, room, "heap.solve", ticket) ;
    } else {
      res.send("il faut fournir un ticket pour obtenir une correction") ;
    }
  })

  robot.respond(/exam\s*([^\s]*)/i, (res) => {
    room = res.envelope.user.roomID
    let query_url = `http://localhost:5000/exam.create?room=${room}`
    if(res.match[1].length > 0) {
      query_url = `${query_url}&ticket=${res.match[1]}`
    }
    robot.http(query_url).get()( (err, resp, body) => {
      console.log(`generated exam with ticket ${body}`)
    })
    if(Math.random() < 0.2) {
      res.send("Vous ne voudriez pas plutôt faire un Scrabble ?")
    }
  })

  robot.respond(/exo conversion binaire\s*([^\s]*)/i, (res) => {
    room = res.envelope.user.roomID
    ticket = res.match[1]
    exomaker_query_image(robot, room, "binconv.create", ticket) ;
  })

  robot.respond(/solution conversion binaire\s*([^\s]*)/i, (res) => {
    room = res.envelope.user.roomID
    ticket = res.match[1]
    if(ticket.length > 0) {
      exomaker_query_image(robot, room, "binconv.solve", ticket) ;
    } else {
      res.send("il faut fournir un ticket pour obtenir une correction") ;
    }
  })

  robot.respond(/dessine l'arbre binaire (.*)/i, (res) => {
    room = res.envelope.user.roomID
    data_string = res.match[1]
    if(data_string.length > 0) {
      console.log(data_string)
      data = JSON.parse(data_string)
      console.log(data)
      let query_url = "http://localhost:5000/btree.draw"
      robot.http(query_url)
        .header('content-Type', 'application/json')
        .post(JSON.stringify(data))( (err, resp, body) => {
          if(err) {
            console.error(err) ;
            return ;
          }
          console.log(body)
          answer = JSON.parse(body) ;
          image = answer.data ;
          upload_base64_png(robot, room, {
            contents : image,
          }) ;
          console.log(`generated binary tree from ${data}`) ;
        })
    } else {
      res.send("il faut fournir les données de l'arbre à dessiner au format [valeur, [gauche], [droite]]") ;
    }
  })
}
