// Description:
//    Pour faire la pelle
//
// Dependencies:
//   None
//
// Configuration:
//   None
//
// Commands:
//   fais la pelle
//
// Notes:
//   Version 1.0
//
// Author:
//   Corentin Arnould

module.exports = (robot) => {
    robot.respond(/fais la pelle/i, (res) => {
	res.send(`\`\`\`
    ___
    \\_/
     |
     |
     |
     |
     |
     |
   __|__
  |  ^  |
  \\     /
   '._.'
\`\`\``)
    })
}
