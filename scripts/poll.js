// Description:
//   Polls.
//
// Dependencies:
//   None
//
// Configuration:
//   None
//
// Commands:
//   oui ou non / sondage : - < adds thumbs up and down reactions for others to click >
//   [texte du sondage] au choix : [option 1] ; [option 2] ; [option 3] - < creates a poll with reactions to click>
//
// Notes:
//   Version 1.0
//
// Author:
//   Vincent Nivoliers

const sleep = async (ms) => {
  return new Promise((resolve) => setTimeout(resolve, ms)) ;
}

const retry = async (fn, n) => {
  for(let i = 0; i < n; ++i) {
    try {
      return await fn() ;
    } catch {
      await sleep(200) ;
    }
  }
  throw ("unable to sucessfully run function") ;
}

module.exports = (robot) => {

  robot.hear(/oui ou non|[sS]ondage\s*:/i, (res) => {
    const id = res.message.id ;
    robot.adapter.driver.setReaction(":thumbsup:", id) ;
    robot.adapter.driver.setReaction(":thumbsdown:", id) ;

    const gender = [
      'man',
      'woman'
    ] ;

    const tone = [
      'medium_light',
      'light',
      'medium',
      'medium_dark',
      'dark'
    ]

    const shrug = `:${res.random(gender)}_shrugging_${res.random(tone)}_skin_tone:` ;

    robot.adapter.driver.setReaction(shrug, id)
  })

  robot.hear(/^(.*) [aA]u choix\s*:\s*([^;]+(?:;[^;]+)+)[\n]*$/, async (res) => {

    /* credit for this function goes to
     * https://github.com/tolstoyevsky/hubot-vote-or-die 
     * */

    const emojis = Array.from(
      {length : 26}, 
      (_,i) => ":letter" + String.fromCharCode(97 + i) + ":"
    )

    //list of options
    const matches = res.match[2].split(';') ;

    if (matches.length > emojis.length) {
      res.send("Il y a trop d'options pour que j'en fasse un sondage, désolée.")
      return
    } ;

    const id = res.message.id ;
    const room = res.message.room ;

    //build poll legend
    let options = [] ;
    for(const match of matches) {
      const trimmed = match.trim() ;
      if (trimmed) {
        //append emoji and option
        options.push(`${emojis[options.length]} ${trimmed}`) ;
      }
    } ;

    // Compose and send the poll message (containing the question and options).
    const msg  = `${res.match[1].trim()}\n\n>>>\n${options.join('\n')}\n\n<<<` ;

    //send the message via driver to get the promise
    const ack = await robot.adapter.driver.sendToRoom(msg, room) ;
    //the message was successfully sent
    const msg_id = ack._id ;

    for(const index of options.keys()) {
      //add reactions in order, after 300ms to avoid error in log
      await retry( () => {
        return robot.adapter.driver.setReaction(emojis[index], msg_id) ;
      }, 25) ;
    }
  })

  robot.respond(/fais l'appel(?:\s*en ([0-9]+) secondes)?(?:\s*avec ([^\s]+))?/i, async (res) => {
    const room = res.message.room ;
    //send the message via driver to get the sent message with its id
    const msg = await robot.adapter.driver.sendToRoom("Cliquez ci-dessous pour signaler votre présence", room)
    const msg_id = msg._id ;

    try {
      //add the reaction for voting
      await retry(() => {
        return robot.adapter.driver.setReaction(':point_up:', msg_id) ;
      }, 25) ;

      //get the message back with its contents
      let poll = await retry( () => {
        return robot.adapter.api.get(`chat.getMessage?msgId=${msg_id}`)
      }, 25) ;

      try {
        //add a counter with the provided time interval, defaults to 30s
        const timer = res.match[1] ? parseInt(res.match[1]) : 120
        let modified = {}
        modified._id = poll.message._id ;
        for(let i = timer; i > 0; --i) {
          //modify the counter to indicate remaining time
          modified.msg = poll.message.msg + `\n\nil vous reste *${i} seconde${i > 1 ? 's' : ''}*` ;
          robot.adapter.driver.editMessage(modified) ;
          //wait one more second
          await sleep(1000) ;
        }
      } catch(err) {
      }

      //close the poll
      let final_msg = {} ;
      final_msg._id = poll.message._id ;
      final_msg.msg = "Merci d'avoir renseigné votre présence ! L'appel est clos."
      await robot.adapter.driver.editMessage(final_msg) ;

      //gather the votes
      poll = await retry( () => {
        return robot.adapter.api.get(`chat.getMessage?msgId=${msg_id}`)
      }, 25) ;

      //send a list of voters in private to the issuer
      const target_user = res.message.user.name ;
      const answers = poll.message.reactions[':point_up:'].usernames;
      //format the list of answers fitering out the robot
      const presence = res.match[2] ? res.match[2] : "V" ;
      const robot_match = new RegExp(robot.adapter.settings.username, 'i') ;
      const csv = answers
      .filter((username) => { return !robot_match.test(username) })
      .map((username) => `${username}\t${presence}`)
      .join('\n') ;

      //get the time of the poll and format it
      const timestamp = new Date(Date.now())
      const time = timestamp.toLocaleDateString('fr-FR', {
        weekday : 'long', 
        day : 'numeric', 
        month : 'long', 
        hour : "numeric", 
        minute : 'numeric'
      }) ;

      //format the private message
      const header = `appel réalisé dans le salon *${room}* le *${time}*`
      robot.adapter.driver.sendDirectToUser(
        header + "\n```" + csv + "```"
        , target_user
      ) ;
    } catch(err) {
      console.error(err) ;
    }
  })
}
