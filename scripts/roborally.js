// Description:
//   Providing algorithmics exercises
//
// Dependencies:
//   None
//
// Configuration:
//   None
//
// Commands:
//   hubot roborally show <name of the board, `big` or `default`>
//   hubot roborally path <board name> <from as line,column,orientation> <to as line,column[,orientation]>
//
// Notes:
//   Version 1.0
//
// Author:
//   Vincent Nivoliers

const tmp = require('tmp')
const fs = require('fs')

const upload = (robot, room, data) => {
  //data layout
  //{
  //  contents : filedata to upload
  //  format : format of the data, passed to Buffer.from
  //  mime : mime type of the data
  //  filename : name of the destination file on the chat
  //  options : {
  //    key : value
  //  }
  //}
  
  //thanks to https://gist.github.com/tanaikech/40c9284e91d209356395b43022ffc5cc

  //build formdata for the data
  const uid = require('uid-safe') ;
  let boundary = uid.sync(18) ;
  let form = "" ;

  //options
  if(data.options) {
    for(const opt in data.options) {
      if({}.hasOwnProperty.call(data.options, opt)){
        form += `--${boundary}\r\n` ;
        form += `Content-Disposition: form-data; name="${opt}";\r\n\r\n${data.options[opt]}\r\n` ;
      }
    }
  }

  //file upload
  form += `--${boundary}\r\n` ;
  form += `Content-Disposition: form-data; name="file"; filename=${data.filename}\r\n` ;
  form += `Content-Type: ${data.mime}\r\n\r\n` ;

  const payload = Buffer.concat([
    Buffer.from(form, 'utf-8'),
    Buffer.from(data.contents, data.format),
    Buffer.from(`\r\n--${boundary}--\r\n`, 'utf-8')
  ])

  //not able to post fully through robot.adapter.api, the payload is messed up
  //api login
  robot.adapter.api.login().then( (ack) => {
    //api upload
    robot.http(robot.adapter.api.url)
    .header('X-User-Id', ack.data.userId)
    .header('X-Auth-Token', ack.data.authToken)
    .path(`rooms.upload/${room}`)
    .header('Content-Type', `multipart/form-data; boundary=${boundary}`)
    .post(payload)((err, response, body) => {
      if(err) {
        error.log(err) ;
      }
    })
  }) ;
}

const upload_base64_png = (robot, room, data) => {
  data.format = data.format || 'base64' ;
  data.mime = data.mime || 'image/png' ;
  data.filename = data.filename || "image.png" ;
  upload(robot, room, data) ;
}

const upload_base64_gif = (robot, room, data) => {
  data.format = data.format || 'base64' ;
  data.mime = data.mime || 'image/gif' ;
  data.filename = data.filename || "image.gif" ;
  upload(robot, room, data) ;
}

module.exports = (robot) => {

  robot.respond(/roborally show ([^\s]+)/i, (res) => {
    room = res.envelope.user.roomID
    console.log("handling roborally show query")
    robot.http(`http://localhost:8081/board?name=${res.match[1]}`).get()( (err, resp, body) => {
      if(resp.statusCode != 200) {
        res.reply(`Nope : ${body}`) ;
      } else {
        let data = JSON.parse(body) ;
        let image = data.png ;
        upload_base64_png(robot, room, {
          contents : image,
          options : {
            msg : `plateau : \`${res.match[1]}\``
          }
        }) ;
      }
    })
  })

  robot.respond(/roborally path ([^\s]+)\s*([,0-9]+)\s*([,0-9]+)/i, (res) => {
    room = res.envelope.user.roomID
    console.log("handling roborally path query")
    robot.http(`http://localhost:8081/path?board=${res.match[1]}&from=${res.match[2]}&to=${res.match[3]}`).get()( (err, resp, body) => {
      if(resp.statusCode != 200) {
        res.reply(`Nope : ${body}`) ;
      } else {
        let data = JSON.parse(body) ;
        if(!data.success) {
          res.reply(`aucun chemin trouvé pour rejoindre \`${res.match[3]}\` depuis \`${res.match[2]}\``) ;
        } else {
          const moves = [
            "avancer de 1", 
            "avancer de 2", 
            "avancer de 3", 
            "reculer", 
            "tourner à gauche", 
            "tourner à droite", 
            "demi-tour"
          ] ;
          let msg = `sur \`${res.match[1]}\` ` ;
          msg += `je vais de \`${res.match[2]}\` ` ;
          msg += `à \`${res.match[3]}\` ` ;
          msg += `en ${data.distance} coups` ;
          if(data.distance > 0) {
            msg += " : \n```" ;
            for(const move of data.path) {
              msg += `- ${moves[move]}\n` ;
            }
            msg += "```" ;
          }
          let image = data.gif ;
          upload_base64_gif(robot, room, {
            contents : image,
            options : {
              msg : msg
            }
          }) ;
        }
      }
    })
  })

}
